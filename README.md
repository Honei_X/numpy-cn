# NumPy 中文文档

## 1. 概述
原始的文档由teadocs 进行翻译。
自己在原始文档的基础上进行阅读与理解。

## 2. 主页
<p align="center">
    <img width="200" height="200" src="./_logo/logo.png" alt="numpy中文文档logo" />
</p>

> Chinese (zh-cn) translation of the NumPy docs
> 
> numpy官方文档的中文版本

当前版本 ``v1.17`` 。

更多信息请直接访问网站：[https://www.numpy.org.cn/](https://www.numpy.org.cn/)

## 版权信息

除非额外说明，本网站的所有公开文档均遵循 [署名-非商业性使用-相同方式共享 3.0 中国大陆 (CC BY-NC-SA 3.0 CN)](https://creativecommons.org/licenses/by-nc-sa/3.0/cn/) 许可协议。任何人都可以自由地分享、修改本作品，但必须遵循如下条件：

- 署名：必须提到原作者，提供指向此许可协议的链接，表明是否有做修改
- 非商业性使用：不能对本作品进行任何形式的商业性使用
- 相同方式共享：若对本作品进行了修改，必须以相同的许可协议共享


## 3. 文档目录
所有的翻译文档都存放在 docs 这个文件夹下面。
每个文件夹的功能作用如下：
| 文件夹 | 功能 |
|------|----|
| docs/about |numpy 介绍| 
|docs/alopecia | 防脱发 😓|
|docs/reference | API 文档 |
|docs/reference/arrays| 数组文档 |
|docs/reference/routines | 常用 API |

